import API
import sys
import math

def log(string):
    sys.stderr.write("{}\n".format(string))
    sys.stderr.flush()

#cells = [[15 for i in range(16)] for i in range(16)]
cells = [[5,4,4,4,4,4,4,4,4,4,4,4,4,4,4,6],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,7]]

def main():
    log("Running...")
    API.setColor(0, 0, "G")
    API.setText(0, 0, "start")

    # If you want to print the cells matrix, use the following:
    # print(*cells, sep='\n', file=sys.stderr)

    x,y,orient = 0,0,0
    
    while True:

        updateWalls(x,y,orient,API.wallLeft(), API.wallRight(), API.wallFront())
        
        # ADD YOUR CODE HERE


def updateWalls(x,y,orient,L,R,F):
    '''
    update the cells matrix depending on the position of the mouse (x,y,orient) and the walls found (L,R,F)
    '''
    if(L and R and F):
        if (orient==0): 
            cells[y][x]= 13
        elif (orient==1): 
            cells[y][x]= 12
        elif (orient==2): 
            cells[y][x]= 11
        elif (orient==3): 
            cells[y][x]= 14

    elif (L and R and not F):
        if (orient==0 or orient== 2): 
            cells[y][x]= 9
        elif (orient==1 or orient==3): 
            cells[y][x]= 10

    elif (L and F and not R):
        if (orient==0): 
            cells[y][x]= 8
        elif (orient==1): 
            cells[y][x]= 7
        elif (orient==2): 
            cells[y][x]= 6
        elif (orient==3): 
            cells[y][x]= 5

    elif (R and F and not L):
        if (orient==0): 
            cells[y][x]= 7
        elif (orient==1): 
            cells[y][x]= 6
        elif (orient==2): 
            cells[y][x]= 5
        elif (orient==3): 
            cells[y][x]= 8

    elif(F):
        if (orient==0): 
            cells[y][x]= 2
        elif (orient==1): 
            cells[y][x]= 3
        elif (orient==2): 
            cells[y][x]= 4
        elif (orient==3): 
            cells[y][x]= 1

    elif(L):
        if (orient==0): 
            cells[y][x]= 1
        elif (orient==1): 
            cells[y][x]= 2
        elif (orient==2): 
            cells[y][x]= 3
        elif (orient==3): 
            cells[y][x]= 4

    elif(R):
        if (orient==0): 
            cells[y][x]= 3
        elif (orient==1): 
            cells[y][x]= 4
        elif (orient==2): 
            cells[y][x]= 1
        elif (orient==3): 
            cells[y][x]= 2


def isAccessible(x,y,x1,y1):
    '''
    return True if there is no wall between (x,y) and (x1,y1)
    '''
    if (x1 < 0 or x1 > 15 or y1 < 0 or y1 > 15): return False
    if (y1==y):
        if(x1>x):
            return cells[y][x] not in [3,6,7,9,11,12,13,16]
        else:
            return cells[y][x] not in [1,5,8,9,11,13,14,16]
            
    elif (x1==x):
        if(y1>y):
            return cells[y][x] not in [2,7,8,10,12,13,14,16]
        else:
            return cells[y][x] not in [4,5,6,10,11,12,14,16]

def show(distances, cells):
    for x in range(16):
        for y in range(16):
            API.setText(x,y,str(distances[y][x]))
            if cells[y][x] in [2, 7, 8, 10, 12, 13, 14, 16]:
                API.setWall(x,y,'n')
            if cells[y][x] in [3, 6, 7, 9, 11, 12, 13, 16]:
                API.setWall(x,y,'e')
            if cells[y][x] in [4, 5, 6, 10, 11, 12, 14, 16]:
                API.setWall(x,y,'s')
            if cells[y][x] in [1, 5, 8, 9, 11, 13, 14, 16]:
                API.setWall(x,y,'w')

if __name__ == "__main__":
    main()