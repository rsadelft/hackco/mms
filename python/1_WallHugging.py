import API
import sys

def log(string):
    sys.stderr.write("{}\n".format(string))
    sys.stderr.flush()

def main():
    log("Running...")
    API.setColor(0, 0, "G")
    API.setText(0, 0, "start")
    while True:
        ## Check walls:
        # if API.wallLeft():
        # if API.wallFront():
        
        ## Move:
        # API.turnLeft()
        # API.turnRight()
        # API.moveForward()

        ## Set color in cell (x,y) to c (k: Black, b: Blue, a: Gray, c: Cyan, g: Green, o: Orange, r: Red, w: White, y: Yellow):
        # API.setColor(x, y, "c")
        ## Set text in cell (x,y)
        # API.setText(x, y, "Text")
        pass

if __name__ == "__main__":
    main()