import API
import sys
import math

def log(string):
    sys.stderr.write("{}\n".format(string))
    sys.stderr.flush()

cells = [[5,4,4,4,4,4,4,4,4,4,4,4,4,4,4,6],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3],
        [8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,7]]
distances = [[math.inf for i in range(16)] for i in range(16)]

def main():
    log("Running...")
    API.setColor(0, 0, "G")
    API.setText(0, 0, "start")

    # If you want to print the cells matrix, use the following:
    # print(*cells, sep='\n', file=sys.stderr)

    toVisit = [(7, 7), (7, 8), (8, 7), (8, 8)]
    for i in toVisit:
        distances[i[1]][i[0]] = 0

    while len(toVisit) > 0:
        # ADD YOUR CODE HERE

        show()
    log("Finished")

def isAccessible(x,y,x1,y1):
    '''
    return True if there is no wall between (x,y) and (x1,y1)
    '''
    if (x1 < 0 or x1 > 15 or y1 < 0 or y1 > 15): return False
    if (y1==y):
        if(x1>x):
            return cells[y][x] not in [3,6,7,9,11,12,13,16]
        else:
            return cells[y][x] not in [1,5,8,9,11,13,14,16]
            
    elif (x1==x):
        if(y1>y):
            return cells[y][x] not in [2,7,8,10,12,13,14,16]
        else:
            return cells[y][x] not in [4,5,6,10,11,12,14,16]

def show():
    for x in range(16):
        for y in range(16):
            API.setText(x,y,str(distances[y][x]))
            if cells[y][x] in [2, 7, 8, 10, 12, 13, 14, 16]:
                API.setWall(x,y,'n')
            if cells[y][x] in [3, 6, 7, 9, 11, 12, 13, 16]:
                API.setWall(x,y,'e')
            if cells[y][x] in [4, 5, 6, 10, 11, 12, 14, 16]:
                API.setWall(x,y,'s')
            if cells[y][x] in [1, 5, 8, 9, 11, 13, 14, 16]:
                API.setWall(x,y,'w')

if __name__ == "__main__":
    main()