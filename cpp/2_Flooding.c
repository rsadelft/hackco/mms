#include <iostream>
#include <string>

#include "API.h"

void log(const std::string& text) {
    std::cerr << text << std::endl;
}

int main(int argc, char* argv[]) {
    log("Running...");
    API::setColor(0, 0, 'G');
    API::setText(0, 0, "start");
    while (true) {
        //// Check for walls:
        // if (API::wallLeft()) {}
        // if (API::wallRight()) {}

        //// Move
        // API::turnLeft();
        //// API::turnRight();
        // API::moveForward();
        
        //// Debugging
        //// Set color of cell (x,y) to c (k: Black, b: Blue, a: Gray, c: Cyan, g: Green, o: Orange, r: Red, w: White, y: Yellow):
        // API::setColor(x, y, 'c')
        //// Set text in cell (x,y)
        // API::setText(x, y, "text")
    }
}