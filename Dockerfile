FROM gitpod/workspace-full-vnc
RUN sudo apt-get update && \
    sudo apt-get install -y libgtk-3-dev libqt6openglwidgets6 libxcb-shape0-dev libxcb-keysyms1-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-cursor-dev libxcb-render-util0-dev x11-xkb-utils libxkbcommon-x11-dev && \
    sudo rm -rf /var/lib/apt/lists/*
COPY "bin/mms.desktop" "/usr/share/applications/mms.desktop"
COPY "mms.conf" "/home/gitpod/.config/mackorone/mms.conf"
